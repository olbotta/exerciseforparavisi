import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

public class subArrayCounterTests {

    @ParameterizedTest(name = "{index} → {0} has {1} cont arrays")
    @CsvSource(textBlock = """
            '0,1,0,0,1', 4
            '0,1,0,1', 4
            '1,0,0,1,0', 4
            '1,0,0,1,1,0', 6
            '1,0,1', 2
            '1,0,1,1', 2
            '0,1', 2
            '0,1,0', 2
            '0,0,1,0,0,0,1,1', 6
            '0,0,0,0,0', 0
            '1,1', 0
            '1', 0
            '', 0
            """)
    public void test(String input, int expected) {

        //SETUP
        int[] array = new int[0];
        try {
            array = Stream.of(input.split(",")).mapToInt(Integer::parseInt).toArray();
        } catch (Exception ignored) {
        }
        //EXERCISE
        int out = subArrayCounter.countLongestContiguousSubArray(array);
        //VERIFY
        assertThat(out).isEqualTo(expected);
    }
}
