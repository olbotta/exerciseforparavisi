import java.util.Arrays;

public class subArrayCounter {
    //Given a binary array nums, return the maximum length of a contiguous subarray with an equal number of 0 and 1.
    public static int countLongestContiguousSubArray(int[] array) {

        int[] n = countNumberOf0and1(array);
        if (n[0] == n[1])
            return n[0] * 2;
        else {
            int left = countLongestContiguousSubArray(Arrays.copyOfRange(array, 0, array.length - 1));
            int right = countLongestContiguousSubArray(Arrays.copyOfRange(array, 1, array.length));
            return Math.max(left, right);
        }
    }

    public static int[] countNumberOf0and1(int[] array) {

        int[] counters = {0, 0};
        Arrays.stream(array).forEach(e -> {
            counters[e]++;
        });
        return counters;
    }
}